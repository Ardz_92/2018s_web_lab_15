<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Exercise 01</title>
</head>
<style>
    tr:nth-child(even) {
        background-color: #f2f2f2
    }
    thead, td {
        border: 1px solid;
    }
</style>
<body>
<table>
    <thead>
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Description</th>
        <th>Time</th>
    </tr>
    </thead>
    <c:forEach items="${AccessLogs}" var="accessLog">
        <tr>
            <td>${accessLog.id}</td>
            <td>${accessLog.name}</td>
            <td>${accessLog.description}</td>
            <td>${accessLog.time}</td>
        </tr>
    </c:forEach>
</table>
</body>
</html>
