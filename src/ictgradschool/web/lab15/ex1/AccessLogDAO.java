package ictgradschool.web.lab15.ex1;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class AccessLogDAO {
    private final Connection conn;

    public AccessLogDAO(Connection conn) {

        this.conn = conn;
    }

    public List<AccessLog> allAccessLogs() throws SQLException {

        List<AccessLog> accessLogs = new ArrayList<>();

        // Creates a statement to use to access the database.
        try (Statement stmt = conn.createStatement()) {

            // Executes the given SQL code and loads the result into a ResultSet object.
            try (ResultSet results = stmt.executeQuery("SELECT * FROM access_log")) {

                // Loop through all rows in the result set.
                while (results.next()) {

                    AccessLog accessLog = getAccessLogFromResultSet(results);
                    accessLogs.add(accessLog);
                }
            }
        }
        return accessLogs;
    }

    private AccessLog getAccessLogFromResultSet(ResultSet results) throws SQLException {
        int id = results.getInt("id");
        String name = results.getString("name");
        String description = results.getString("description");
        Timestamp time = results.getTimestamp("time");

        return new AccessLog(id, name, description, time);
    }

    public void addAccessLog(AccessLog a) throws SQLException {
        try (PreparedStatement stmt = conn.prepareStatement("INSERT INTO access_log ( name, description) VALUES ( ?, ?)", Statement.RETURN_GENERATED_KEYS)) {

            stmt.setString(1, a.getName());
            stmt.setString(2, a.getDescription());

            int numRows = stmt.executeUpdate();

            ResultSet rs = stmt.getGeneratedKeys();
            rs.next();

            int auto_id = rs.getInt(1);
            a.setId(auto_id);

            Timestamp update_time = rs.getTimestamp(4);
            a.setTime(update_time);
        }
    }
}
