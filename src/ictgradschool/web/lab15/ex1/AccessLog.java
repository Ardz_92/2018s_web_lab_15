package ictgradschool.web.lab15.ex1;

import java.io.Serializable;
import java.sql.Timestamp;

public class AccessLog implements Serializable {
    public Integer id;
    public String name;
    public String description;
    public Timestamp time;

    public AccessLog(){ }

    public AccessLog(int id, String name, String description, Timestamp time){
        this.id = id;
        this.name = name;
        this.description = description;
        this.time = time;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }
}
