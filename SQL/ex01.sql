DROP TABLE IF EXISTS access_log;

CREATE TABLE access_log(
  id INT NOT NULL AUTO_INCREMENT,
  name VARCHAR(50),
  description VARCHAR(500),
  time TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id)
);

INSERT INTO access_log (name, description)VALUES
  ('Dewey Dualis', 'This is the first description'),
  ('Ruby Red', 'This is the second description'),
  ('Keith Civic', 'This is the third description'),
  ('Snow White', 'This is the fourth description');